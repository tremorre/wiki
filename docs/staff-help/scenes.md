# Scene coordination resources

This document contains templates for scene moderators and higher staff to make use of when helping a member plan and advertise their scheduled performance.

* Templates
    * [Planning thread](#planning-thread)
    * [Announcements](#announcements)
* Examples
    * [Planning thread](#planning-thread_1)
    * [Announcements](#announcements_1)

## Templates

### Planning thread

This template is workshopped in a `#playwriting` thread with the host of the scene. The finished product is then posted in the `#theatre` thread to serve as the announcement and anchor point for their scene.

```
**TYPE**: private or public
**PREFERRED MODERATOR**: (optional) `@scene-mod`
**PROMOTION REQUESTED?** (public scenes only) whether or not you'd like the moderator to announce your scene in advance with a ping to `@confidant`

**WHEN**: *weekday*, *month day-num* @ *hour tz*
**DURATION**: *duration* ~ *duration* (see point in constraints, if applicable)
**WHO**: `@requestor-ping`, *any others who have expressed interest in actively participating/directing on mic or video*
**WHAT**: *a description of what desired outcome/vibe/progression of the scene is*
**SOLICITATION**: (public scenes only) *a hook for why people might want to play with you*
**INVITEES**: (private scenes only) *a list of* `@users` *that you are comfortable with viewing your scene* (advanced consent not required, only used for addition of users to the #theatre thread)

**CONSTRAINTS**: (*a list of constraints that you predict could potentially arise during the scene, your plan for dealing with them, or how others might be able to*)
- if i get too high, the scene may be delayed for 30m-1h until im in a comfortable enough space to verbalize, think and enjoy myself
- if i go nonverbal, i will type in the #theatre thread instead of using the mic
- i won't have easy access to discord while the scene is ongoing to read and respond to chat, but will be able to hear and talk

**SIGNALES**: (*a list of signals, safe words, gestures, and/or advice for how others ought to respond to you using them in specific potential circumstances*)
- 🟢 green (all good), 🟡 yellow (slow down/check in), 🔴 red (stop, comfort, process)
- [brackets to denote out-of-character considerations like "don't want to disturb roommate" or "don't like that sort of degradation" or current inner state or other such relevant feedback]
- if i signal to stop (red 🔴) because of x, it'd be best if the dominant responded with y

**LIKES:** (*)a list of acts or play styles that would resonate with you within the context of _this_ scene - not a copy paste of all your kinky interests*)
-

**LIMITS**: (*a list of your triggers, and hard or soft limits - probably similar to likes, but if you feel it's important to list everything, please do*)
-

**AFTERCARE**: (*advice on the ways in which participants or onlookers may offer support/praise or otherwise help you conclude the scene in positive spirits*)

**KINK DOC**: *link or attachment*
**MOODBOARD**: *link to the first post in your* #moodboard *thread*
**EVENT LINK**: (moderator to fill) *link to the on-server scheduled event details*
```

### Announcements

**Promotion:**

Public scenes

```
[**SCENE ANNOUNCEMENT**] ✨ *event title* is scheduled for *dow*, date* at *hour tz*!
RSVP with event link if interested in attending!
Post here if interested in participating/directing :relaxed: @confidant
```

Private scenes

```
[**SCENE COMMENCEMENT**] ✨ *event title* scheduled for *dow*, date* at *hour tz*!
RSVP with event link if interested in attending!
Post here if interested in participating/directing :relaxed: @voyeur @invitee
```

**Commencement:**

Public scenes

```
[**SCENE COMMENCEMENT**] ✨ *event title* is now starting! Join 🔉`dungeon` to watch~ @confidant
```

Private scenes

```
**[SCENE COMMENCEMENT] ✨ *event title* is now starting! Join 🔉`dungeon-priv` to watch~ @voyeur @invitee**
```

## Examples

### Planning thread

**WHEN**: Tuesday, November 16th @ 6PM PT

**WHO**: `@a fierce cat`, `@vaunted prize`, `@needy tickleslut`

**WHAT**: the steward of aria will be holding an exhibition of pain; offering herself and the implements she owns to those interested in hurting her, and taking their orders for how to do so.

**IMPLEMENTS**: needle play kit, short single tail whip, leather paddle, hard wooden paddle/ruler, bamboo cane, clover clamps, mini and regular wooden clothespins, mini plastic clothespins, wooden and acrylic nipple sticks

**SIGNALES**: 🟢 Green (all good), 🟡 Yellow (slow down/check in), 🔴 Red (stop, comfort, process)

**LIMITS**:

- *Soft*: Pinching with fingers (clamps OK, sometimes), foot pain, burning, tactile oversensitization
- *Hard*: Hard/fast biting, face slapping, gags, verbal mocking, mean-spirited condescension

**MOODBOARD**: https://discord.com/channels/732379283086901310/904378925310951444/904378927018033152

**EVENT LINK**: https://discord.com/events/732379283086901310/908908983534899272

### Announcements

[**SCENE ANNOUNCEMENT**] *erin's torment* is scheduled for __tomorrow__, at 6PM PT! RSVP with event link if interested in attending! Post here if interested in participating/directing :relaxed: @confidant

[**SCENE ANNOUNCEMENT**] *d's NNN Challenge* is scheduled for Tuesday, 11/23 at 8 PM PT! RSVP with event link if interested in attending! Post here if interested in participating/directing :relaxed: @confidant
