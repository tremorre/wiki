# Underage content

A post featuring sexual depictions of underage persons or any sort is prohibited by Discord's service agreements, and ought to be handled according to our [Moderation Process](../policy/moderation.md). This document serves as a guide to identifying such content.

* Policy
    * [Discord's stance](#discords-stance)
    * [Our interpretation](#our-interpretation)
* [Walled gardens](#walled-gardens)
    * [Pixiv](#pixiv)
    * [Twitter](#twitter)
    * [Tumblr](#tumblr)
    * [exhentai](#exhentai)
* [Identifying infringements](#identifying-infringements)
    * [Reverse image search](#reverse-image-search)
    * [Discerning youthful characteristics](#discerning-youthful-characteristics)
    * [Special cases](#special-cases)

## Policy

### Discord's stance

Depictions of children in a sexual context is against Discord's Community Guidelines. This is not limited to photography of actual children, but also illustrations, and digital productions.

> Do not sexualize children in any way. You cannot share content or links which depict children in a pornographic, sexually suggestive, or violent manner, including illustrated or digitally altered pornography that depicts children (such as lolicon, shotacon, or cub) and conduct grooming behaviors.

─ [Discord Community Guidelines](https://href.li/?https://discord.com/guidelines), Section "Respect Each Other", Point 5. Retrieved 2022-02-25.

Discord considers both appearance, and canonical age when ascertaining whether or not a drawn character is allowed to be hosted on their services.

> Explicitly ban images of characters that are canonically 2000 year old vampires that look 12, and content containing fictional characters that are canonically minors but may look older, etc.

─ [Discord Moderator Academy](https://href.li/?https://discord.com/moderation), Page "[405: Practicalities of Moderating Adult Channels](https://href.li/?https://discord.com/moderation/1500000179641-405:-Practicalities-of-Moderating-Adult-Channels)", Section "Channel Rules". Retrieved 2022-02-26.

### Our interpretation

Let's look at a scenario posed to senior staff by a moderator who was new to the team:

> Does discord seriously have it both ways about this? What is the discord age of majority; is it related to the universe/country in the work? Location of the poster? Location of discord hq?
What if they age over the course of the series & a work could plausibly be of the character at either 17 or 27
>
> I want to know the rules about posting [this guy's](https://cdn.discordapp.com/attachments/942924852501835837/952255137857028146/Jotaro_SC_Infobox_Manga.png) (Jotaro JoStar) cock shots. It's actually important since it informs lots of posts
>
> ![Jotaro JoStar](https://i.imgur.com/p7JmQdZ.png)
>
> Do we have leeway whatsoever, is this about "making a case" or is it about discord admins looking at posts w/ the goal of finding a basis to nuke servers
>
> Like I'm not talking about loliposting I'm talking about how most anime people are canonically 17
>
> Offserver, in other cultures, "is jotaroposting, shota? Discuss" is an actual discourse issue people disagree on in large #s
>
> I think its worth phrasing that rule based on exactly what the goal of the rule is & what your stance is or isn't on that issue

The spirit of Discord's stance may be easily interpreted with the help of common threat models informed by modern ethics. We augment that by also considering practical consequences for a business service  when addressing this and questions like it.

> its about how the character appears mostly. but if the character appears borderline and they are canonically 15, then i would delete the image
>
> if i know that the jotaro depicted in a media post is 17, i would delete the post. i wouldnt likely delete a lewd depiction where he looked like this if i didn't recognize him as straight out of the manga's time period where he's underage.
>
> and yet, jotaro doesnt look 17 to start with, so unless one is intimately familiar with the franchise, or goes to great investigatory effort of their own volition to figure that out, its unreasonable that we'd know. if a user _reports_ such information to you, delete the post.
>
> that said, if he's noticeably aged up, it's permissible.
>
> in any case, my priority is young characters that have a canonical age of 13-15 or lower. those are the characters that we can make judgment calls for, easily, based on what we can see and know. like marnie from pokemon. she has no canonical age but is typically regarded as preteen, considering the audience of the franchise. she cannot be posted if she looks like she does in the tv shows or manga.

## Walled gardens

Some sites require registration to view R-18 content, or access their media at all.

While our system operators try to reduce moderation burden through bot integrations that post previews for non-natively embeddable links, these features are a work in progress and, at times, unreliable. View the status of preview features implemented in `cables` at its [issue tracker](https://gitlab.com/aria.theater/cables/-/issues?sort=created_date&state=opened&label_name[]=focus/previews).

We strongly recommend that all moderators create accounts on these sites, proactively, and learn how to use them as part of their initial onboarding.

### Pixiv

1. [Register](https://href.li/?https://accounts.pixiv.net/signup) for an account
2. Confirm your email
3. [Log in](https://href.li/?https://accounts.pixiv.net/login), if necessary
4. Access the [user settings](https://www.pixiv.net/setting_user.php) page
5. Toggle 'Explicit content (R-18)' to 'Show'
6. Toggle 'Ero-guro content (R-18G)' to 'Show'
7. Select 'Update' to submit the form

### Twitter

1. [Register](https://href.li/?https://twitter.com/i/flow/signup) for an account
2. Confirm your phone number and email address, as necessary
3. [Log in](https://href.li/?https://twitter.com/i/flow/login), if necessary
4. More → [Settings & privacy](https://twitter.com/settings/account) → Privacy and safety → [Content you see](https://twitter.com/settings/content_you_see)
5. Check 'Display media that may contain sensitive content'

### Tumblr

1. [Register](https://href.li/?https://www.tumblr.com/register) for an account
2. Confirm your email address
3. [Log in](https://href.li/?https://www.tumblr.com/login), if necessary
4. Access the [user settings](https://href.li/?https://www.tumblr.com/settings/account) page
5. Toggle 'Hide Sensitive Content' off

### exhentai

It used to be that accessing this site was onerous and required manual cookie fuckery or a browser addon. This ought to no longer be the case, thankfully.

Browser access:

1. [Register](https://href.li/?https://forums.e-hentai.org/) an account on e-hentai forums
2. Confirm your email
3. Wait 7 days to be automatically granted exh access
4. Log into your eHentai forum account, if necessary
5. Attempt to [access](https://href.li/?https://exhentai.org) the exhentai site in the same browser session
6. If you are presented with a white page, repeat steps 2-5 from an Incognito session
7. If you still aren't able to get in, try a browser addon:
    * [Chrome](https://github.com/HitomaruKonpaku/SadPanda) extension 'SadPanda'
    * [Firefox](https://addons.mozilla.org/en-US/firefox/addon/exhentai-passport/) addon 'exhentai passport'

Android app:

* Download and manually install [TachiyomiSY](https://github.com/jobobby04/TachiyomiSY) from APK
* Configure exhentai access from the settings
* Search the gallery link, or name in the main app view

## Identifying infringements

Our users typically make an effort not to post obviously underage media. They have, at time of writing, never posted actual real-life child pornography, but some have accidentally posted illustrations of underage-appearing or borderline fictional characters.

Most do not cross-check the content they find on other Discord servers or subreddits before posting them here, trusting that moderation at those sources have done their own duty enough to assure the content is safe to cross-post. While this is a reasonable assumption to make, other communities have different standards, and moderation isn't always perfect.

It is [your duty](../policy/moderation.md#observation) as a moderator to expand thumbnails, open image links and view videos to identify infringements.

There are several methods to accomplish this:

- Find the source, and look at tags, descriptions, and other creator-provided information
- Search the web for the character or franchise in question
- Use knowledge of human anatomical development to judge bodily characteristics

### Reverse image search

These tools are useful for finding the source on Pixiv, or mirrors of it on boorus. From there, you may look at tags, descriptions, and explore the author's other works. Service integrations that you'll be interested in making use of for drawn media include [IQDB](https://href.li/?https://iqdb.org) and [SauceNao](https://href.li/?saucenao.com/).

1. [Browser addon](https://href.li/?https://github.com/dessant/search-by-image)
2. [Web utility](https://href.li/?https://imgops.com)
3. SauceNao query via :paintbrush: react

### Discerning youthful characteristics

Traits reliable for positive identification:

- Hip-to-waist proportions
- Anatomical development of the clitoris
- Definition of face and jawline

Traits that might help in combination with the above:

- Fat distribution and concentration, for fems, when the character isn't otherwise adult-appearing nor obviously undernourished
- Stature/height, especially if conceivably under 5'
- Rounded jawline or cheeks, rather than softly curved/angled

Unhelpful traits to factor in:

- Breast size, including flat-chested people
- Hand or foot size
- "Petite" aka lithe of frame

### Special cases

The criteria above might not be so helpful in some cases:

- Mythical creatures of short stature/slight build such as fairie, halflings, goblins, etc.
- "Feminine boys," who may or may not be on HRT, and are conceivably in late puberty or adults according to anatomical development
- Anthropomorphizes creatures such as animals or hybrids in furry artwork

You can always consult other staff in `#janitorium` if you're unsure about a given piece of media, and doing so is **strongly encouraged**.

Content such as this may fare better at being identified with a more specialized tool:

- [e621](http://href.li/?https://e621.net) furry booru
- [Derpibooru](https://href.li/?https://derpibooru.org) pony booru

Most boorus can be searched by MD5 checksum of the media e.g. `md5:CHECKSUM_HERE`. For Derpibooru specifically, this functionality isn't implemented, but instead there is reverse image search functionality via upload.