# Bots

We have a few stable bots for general use on the server. This document describes some of their features, commands and lists external resources to learn more.

* Commands
    * [Name thralls](#name-thralls)
    * [Intra-channel message links](#intra-channel-message-links)
    * [Thread auto-subscription](#thread-auto-subscription)
* [Reactions](#reactions)
* Additional resources
    * [Fletcher](#fletcher)
    * [cables](#cables)
    * [PluralKit](#pluralkit)

## Commands

### Name thralls

Anyone may toggle the name thrall feature roles with the `.namethrall` command. If you're presently not a `namethrall`, it will make you one, otherwise, it'll revert you to a `namedom`.

If you are presently a `namethrall`, you will not be able to change your fellow thrall's nick with the client-native Rename feature. This is (un)fortunately a consequence of Discord's role and permission structure. Instead, use the `.nick @user "new nickname"` command.

### Intra-channel message links

Use the following command to move a discussion from one channel to another.

```
!tp #channel [topic/reason]
```

This will create a link where the command was executed pointing to the destination channel, and vice versa.

### Thread auto-subscription

If you would like to be subscribed to all new public threads as they are created, this may be achieved with `@Fletcher`.

Elect the `robot ally` role in `#registration`, enter `#kiosk`, and issue this command:

```
!preference use-threads true
```

## Reactions

:eye_in_speech_bubble: — Report the post anonymously to staff, in private (followed by the reaction being removed automatically)

⭐ — Vote for a post to be submitted to the star board in `#showcase`

🖌 — Query SauceNAO with the any media linked or attached to the post

❓ or ❔ — [Get information](https://pluralkit.me/guide/#querying-message-information) about a proxied message's system (sender account, system and member names)

🔔 or ❗ — [Ping](https://pluralkit.me/guide/#pinging-a-specific-user) the user account behind a proxied message

📍 — Pins a message to the channel or thread

🕜 — rot13 decode a message

📡 — Notification of message reacts

📑 — "Bookmark" the post i.e. have the bot DM it to you

📁 — Submit link(s) in message to archival service

:hash: — DM markdown source of a message

❌ — Delete a post e.g. linked message previews; only works if the bot offers the reaction to you

## Additional resources

### Fletcher

While the developer welcomes emails and issues requesting features, she is more likely to prioritize working on them if financially incentivized.

* Help
    * [Documentation](https://man.sr.ht/~nova/fletcher/)
    * DM the bot `!help`
* [Source code](https://git.sr.ht/~nova/fletcher/tree)
    * [Text manipulators](https://git.sr.ht/~nova/fletcher/tree/master/item/text_manipulators.py#L3698)
    * [Message functions](https://git.sr.ht/~nova/fletcher/tree/master/item/messagefuncs.py#L1397)
    * [Command functions](https://git.sr.ht/~nova/fletcher/tree/master/item/commandhandler.py#L3346)
    * [Moderation functions](https://git.sr.ht/~nova/fletcher/tree/master/item/janissary.py#L2188)
* [Support](https://buymeacoffee.com/fletcher/)
* [Contact](https://novalinium.com/)

### cables

Originally written by an aria sysop (Eli). Hosted and contributed to by erin.

We welcome feedback about features you'd like to see in `#sandshaping` or posted to the issue tracker! You may opt-in to this channel in `#registration`.

* Help
    * [Documentation](https://man.sr.ht/~luma_inhibitor/cables/)
    * DM the bot `.help`
    * Post in `#sandshaping`
* [Source code](https://gitlab.com/aria.theater/cables)
* [Issue tracker](https://gitlab.com/aria.theater/cables/-/issues)

### PluralKit

This bot provides message proxying facilities to vary the perceived identity of a post author.

**Use cases**

- multiple people sharing one body (aka "systems")
- roleplaying as different characters without having several accounts
- anyone else who may want to post messages as a different person from the same account

* Help
    * [Quick start](https://pluralkit.me/start/)
    * [User guide](https://pluralkit.me/guide/)
    * [Command list](https://pluralkit.me/commands/)
    * [Support server](https://discord.gg/PczBt78)
* [Source code](https://github.com/xSke/PluralKit)
* [Issue tracker](https://github.com/xSke/PluralKit/issues)
* [Privacy policy](https://pluralkit.me/privacy/)