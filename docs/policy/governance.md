# Governance

A community is nothing without its participants. While staff make considerable effort to be in touch with the local culture and make the best possible decisions with respect to guiding it, determining and overcoming biases can be tricky. In recognition and appreciation of the needs and desires of everyone who would stand to benefit from our space flourishing, your feedback and ideas are important to leadership.

When administrators consider making significant changes to server policy or structure, we usually adhere to a review process. This starts internally amongst the council of administrators and is then taken to the wider userbase. At that time, an announcement will be posted to `#bulletins` directing those with thoughts to make comment in `#patron-services`. Depending on the outcome of the ensuing discussions, the proposed changes may be adopted or retracted.

Once a change is enacted, we consider it to be undergoing trial, and proceed to observe its effects. Continuous feedback is important and encouraged. If you feel we made a mistake, or observe a detrimental outcome, _please_ [let us know](guidelines.md#get-in-touch). If you're uncomfortable with airing your thoughts on the server proper, there are alternate contact methods offered on the previously linked page.
