# Channels

In this document you'll find descriptions of our Discord channels. For channel-specific rules, see [Guidelines](../policy/guidelines.md).

[TOC]

## Lobby

Housekeeping

* `#bulletins` — Staff announcements; community proposals, restructuring, corrective action, etc.
* `#registration` — Self-service react-role grants for gain of function, signaling and channel access

## Main Hall

Non-explicit discussion

* `#intermission` — Main general chat; banter with others, talk about life, or what-have-you
* `#aesthetic` — Luxurious posting zone for sharing cute/pretty things, exciting or otherwise positive experiences
* `#media` — Entertainment arts; anime, tv series, movies, video games, literature, music, etc.
* `#computing` — Programming, operations, networking, consumer products, etc.
* `#biohacking` — Transforming the psyche or its wetware to suite the operator's needs; behavior modification, psychopharmacology, medicine, biochemistry, etc.
* :sound: `#songs` — General non-horny voice chat

## Academy of Arts

Educational and supportive resources and discussion

* `#library` — Curated "best-of" resource lists spanning many topics, contributed by our users, and staff for the purpose of education and personal growth
* `#peer-counseling` — Support, advice, or inquiry on topics spanning relationships, play, or mental health concerns, whether kinky, or not
* :sound: `#auditorium` — Infodumps, seminars, or coaching sessions, whether ad-hoc or pre-scheduled

## Wing of Hedon

Lewd media, interactive play and stimulating chats

* `#moodboard` — Express yourself! A space to put what you want to get out of on-server interactions~
* `#showcase` — Open-access "hall of fame" for screenshots, quotes, message links
* `#rehearsal` — Lewd & kinky general. Banter, explorations, or discussion of praxis, theory, and safety
    * Degree of NSFW ought to be kept around referential, horny-adjacent or suggestive
* `#scripture`  — Links to erotic stories, tweets, reddit posts, and the like
* `#gallery`  — Imagery and video incl. ecchi, hentai, porn, & kink
    * Use threads in the text/image gallery channels to reply to posts you fancy, please!
    * If your comments turn to overt sexting in any of the above three channels, [teleport](bots.md#intra-channel-message-links) to `#theatre`
* `#theatre` — Sexting/erotic roleplay, storytelling, pining, and commentary on dungeon scenes
* :sound: `#bullying` — Horny voice chat involving flirting or light sexual and/or kinky ribbing
* :sound: `#dungeon` — Exhibition of degenerate performative arts
* :sound: `#dungeon-priv` — Private room for limited access degeneracy

## Offices

Community meta

* `#patron-services` — Server meta — help, propositions & cultural discussion
* `#playwriting` — Scene requests & planning
* `#sandshaping` — Community technology planning & development
* `#kiosk` — Bot commands & testing
* :sound: `#pair-programming` — Want to learn or help with our tech? Pair up with someone here!
