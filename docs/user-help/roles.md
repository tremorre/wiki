# Roles

This document details some guidelines surrounding the self-assignable roles available by emoji react in `#registration`.

- Preferences
    - [Direct message willingness](#direct-message-willingness)
    - [Pronouns](#pronouns)
    - [Kinks](#kinks)
- [Behavioral](#behavioral)
- [Functional](#functional)

## Preferences

### Direct message willingness

Most applicable to private conversations, these roles are made available for users to broadcast their comfort with receiving DMs.

In absence of a user electing any of these, use your best judgment, or ask them if if you may DM them first. In the former case, assess the relationship thus far may help. Are they friendly or flirty with you? Do they have the `fluid consent` or `rapeable` role? Have you had several positive conversations in public?

**Do not hesitate to DM staff with server-related concerns!** Irrespective of any preference they've elected for this, When it comes to messaging [staff](staff.md) about server-related matters, these roles don't apply.

### Pronouns

While we offer you the ability to designate these for others to know what pronouns to use for you, this is not a license to be mean to others over not knowing, or messing up.

Try not to assume the gender of others in the case where none is broadcast. When in doubt, ask. Barring this, default to either gender neutral pronouns, their nick, ID, or first name (provided they've revealed it in public).

### Kinks

These are meant to be an adjunct to use of `#moodboard`. They're rather limiting in that they only encompass kinks one is interested in to some extent, and cannot be used to express limits or boundaries, at this time.

To reduce decision paralysis, we've tried to minimize the kinks available by consolidation and omission. Many fantasy-only options were excluded specifically because we recommend these being used to broadcast desires in play with other members.

## Behavioral

The gems seen adjacent to member nicknames in the chat are [role icons](https://href.li/?https://support.discord.com/hc/en-us/articles/4409571023639-Custom-Role-Icons-FAQ) serving as play-related consent indicators.

Members are _strongly encouraged_ to choose one if they take the `confidant` role and interact with others in `#dungeon`. If intending to participate in play there, this is _mandatory_.

## Functional

The name thrall game is a bit of fun involving loss of complete control over one's nickname. Two roles control this behavior:

* `namedom` — Default role added on join. Allows a user to change the nick of someone with the `namethrall` role.
* `namethrall` — Makes the magic happen for the subordinate. Does not prevent the user with the role from changing it themselves.

See the [Bots](bots.md#name-thralls) page for usage instructions.
