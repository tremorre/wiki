# Channel management

This document contains instructions for how to manage the creation and archival of channels.

For a detailed breakdown of our Discord server's permission structure, and the logic behind its implementation, see [Permissions](permissions.md).

[TOC]

## Creation

### Modchat

Until we have modmail implemented in `cables`, we have been manually creating channels in the 'Modchat' category when staff want to have a private, on-server conversation with a member over server-related matters.

To create one of these channels, follow this process:
1. Create the channel under the 'Modchat' category, make it private, and skip role selection
2. Edit the channel, enter the Permissions view, and select 'Advanced'
3. Sync the channel with the category if it isn't already. This ought to add the `steward` and `council` roles with `View channels` and `Manage permissions` granted.
4. For `@everyone`, ensure that `View channel` is denied.
5. Add a member override for who you want to admit to the channel. Grant them the `View channels` permission only.

### Opt-in space

TBD

### General channel

TBD

### Horny channel

TBD

## Archival

It is rare that we will ever delete a channel outright. Typically, the only case for this is if it is never used by our members whatsoever.

There are two types of archival. That which allows public read-only access through the `historian` role, and that which makes the space staff-only.

**Public read-only:** Drag the channel to the 'Lower Level 3' category.

**Staff-only:** Drag the channel to the 'Storage Facility' category.

In either case, you must sync permissions to the category that the channel now resides in.

1. If after dragging the channel over, a modal appears asking if you want to sync permissions to the category, do that.
2. If that modal doesn't show up, edit the channel, select the Permissions view, and sync them to the category.
