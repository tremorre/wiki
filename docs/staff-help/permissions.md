# Server permissions structure

This document explains in depth how our Discord server's permissions are laid out.

For instructions on how to manage channel lifecycles, see [Channels](channels.md).

[TOC]

## Introduction

Read over the following resources for details on how Discord permissions work:

1. [Inheritance and ACL application order](https://href.li/?https://support.discord.com/hc/en-us/articles/206141927)
2. [Guide to role management](https://href.li/?https://medium.com/cbblog/understanding-discord-roles-and-permissions-a1fff3ee07a7)

## Inheritance

Attempt to adhere to the inheritance model, applying permissions from the top down:

1. Roles
2. Categories
3. Channels
4. Channel members

Examples:

* If the channel is to be role-gated, change the channel-specific permissions to deny 'View channel' to `@everyone`, and grant it for the gate role. Only this one dial is required, as many other discrete permissions are implicitly denied in turn.
* If `@everyone` has the permission, and/or it's inherited from some other permission, leave that toggle at default (`/`). That is, if `@everyone` is supposed to be able to view the channel, but not post in it without a specific role, restrict 'Send messages' and related permissions, but not `Attach files` or `Embed links`, since those are implied.
* See screenshots [here](../assets/discord-permissions-readonly-1.png) and [here](../assets/discord-permissions-readonly-2.png).

### Exceptions

In some circumstances, we don't follow this paradigm, intentionally.

The `advisor` role is set up as follows:

* No privileged permissions granted at the role level - fine-grained permissions are applied at the channel level instead
* Almost every channel sets `Manage messages` and `Manage threads` permissions
* `Create invites` is allowed, but only from `#tour`

Some channels have exclusions or overrides for bots; all the voice channels explicitly disallow bots in them, because bot frameworks can facilitate audio recording that we do not necessarily endorse for privacy reasons.

Over half the channels aren't synchronized to their category because of opt-in roles such as `entertained`, and the special case `confidant` role that grants write and access permissions to ERP spaces.

### Adherence

The admin proxy role, `steward`, is a special case staff role, but also an example of where we _do_ follow the inheritance model to the greatest extent possible:

* A subset of moderation permissions are granted at the role level, such as `Manage webhooks` and `Manage channel`
* The permissions granted to the role are left to trickle down to channel-scope by leaving the corresponding permissions set to default (`/`)

This role is only assigned to the community founder's account(s) that do not also hold actual server ownership. It has a couple peculiarities worth noting:

* The `Administrator` and `Manage roles` permissions are _not_ granted to it, in an attempt to avoid accounts assigned to the role being classified as a server administrator
* The channel-scoped `Manage permissions` permission unfortunately isn't implied by a role-level grant of `Manage channel`. Due to this, each channel must have it granted especially for the role 😞
