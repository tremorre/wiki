## Policy

* [Guidelines](policy/guidelines.md)
* [Scenes](policy/scenes.md)
* [Governance](policy/governance.md)
* [Moderation](policy/moderation.md)
* [Matrix](policy/matrix.md)

## For Users

* [Privacy](user-help/privacy.md)
* [Channels](user-help/channels.md)
* [Roles](user-help/roles.md)
* [Bots](user-help/bots.md)
* [Tools](user-help/tools.md)
* [Staff](user-help/staff.md)
* [Matrix](user-help/matrix.md)

## For Staff

* [Scenes](staff-help/scenes.md)
* [Underage content](staff-help/underage-content.md)
* [Sanctions](staff-help/sanctions.md)
* [Permissions](staff-help/permissions.md)
* [Channel management](staff-help/channels.md)
* [Operations](staff-help/operations.md)
