# Tools

* Protecting your identity
    * [Obscuring photos](#obscuring-photos)
    * [Obscuring video](#obscuring-video)
    * [Facial recognition disruption](#facial-recognition-disruption)
* Bookmarklets
    * [Toggle images in Discord](#toggle-images-in-discord)
* [Alternate frontends](#alternate-frontends)
## Protecting your identity

### Obscuring photos

On Android, there is [ObscuraCam](https://href.li/?https://guardianproject.info/apps/obscuracam/). It removes EXIF metadata and allows you to pixelate and redact portions of an image.

On iPhone, [Privee](https://href.li/?https://apps.apple.com/us/app/privee-censor-your-photos/id1056970494) is good for mosaic blur of single photos, and [Hide My Face](https://href.li/?https://apps.apple.com/us/app/hide-my-face/id1530322831) for batches, though it only does black boxes and markings. Neither are all that good at detecting faces unless they're unobscured and forward facing.

[Image Scrubber](https://everestpipkin.github.io/image-scrubber/) ([source](https://github.com/everestpipkin/image-scrubber)) is a web-based tool that does all processing in browser, and hence, works perfectly well offline or locally. It can remove EXIF metadata, blur, and rotate.

On Linux, `gimp` is pretty good for single photo edits. Use the Paths tool to draw a selection around the area a point at a time, then hit enter to confirm. Apply a mosaic blur mask — Filters → Blur → Pixelize. Adjust the Block width and height as necessary.

### Obscuring video

[Video Mosaic](https://href.li/?https://apps.apple.com/us/app/video-mosaic-app/id1185459031) for iPhone's iOS supports automatic face detection, and manual shape positioning.

As of March 2022, automatic face detection doesn't work all that well i.e. it'd miss a few frames here and there.

Keep in mind that when a shape is moved or transformed, that change happens gradually over the period elapsed since last shape change.

- Make sure to review the video each time you alter the shape to make sure it did what you wanted.
- Make small changes if more than a few seconds have passed, and then a large change a frame or so later, to avoid this behavior biting you.

### Facial recognition disruption

[Fawkes](https://github.com/Shawn-Shan/fawkes) will remove characteristics from your selfies that are used by facial recognition software to fingerprint your face.

Helpful for the super paranoid as a countermeasure to Big Tech mining of data stored on their platform and cloud services. Unhelpful in realistically preventing a human from identifying you.

## Bookmarklets

### Toggle images in Discord

Kindly provided by `conifer`, this simple piece of JS will toggle image display in the Discord _web_ client.

**Download**: [toggle-images-bookmarklet.js](https://cdn.catgirl.technology/bin/toggle-images-bookmarklet.js)

**Setup**:
- Open the file in your favorite code editor
- Change the boolean on the last line to control behavior: `true` to show image and `false to hide
- Save two copies of the file, one for each purpose
- See [this guide](https://mreidsma.github.io/bookmarklets/installing.html) ([mirror](https://archive.is/zTp0O)) for adding the bookmarklet to your browser

## Alternate Frontends

| Platform | Alternate                                     | Purpose                                             |
| -------- | --------------------------------------------- | --------------------------------------------------- |
| Twitter  | [nitter.fly.dev](https://href.li/?https://nitter.fly.dev)      | Bypass login for R-18 content, embed media properly |
| Twitter  | [nitter.hu](https://href.li/?https://nitter.hu)                |                                                     |
| Reddit   | [teddit.namazso.eu](https://href.li/?htts://teddit.namazso.eu) | No age gate for R-18 content                        |

Automatic redirection

- [Privacy Redirect](https://github.com/SimonBrazell/privacy-redirect) (Chrome, Firefox)
- [UntrackMe](https://framagit.org/tom79/nitterizeme) (Android)

Instance lists

- [nitter](https://github.com/zedeus/nitter/wiki/Instances)
- [teddit](https://github.com/teddit-net/teddit#instances)
- [libreddit](https://github.com/spikecodes/libreddit#instances)