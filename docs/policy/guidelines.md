# Guidelines

This document describes rules and guidelines that members of aria chat services are expected to have read and understand.

Imperative language is used when the clause constitutes a rule, and suggestions are made for guidelines. In the latter case, we make these suggestions often because there isn't one single way to reach the end state of harmony that we desire, but have found these to be suitable and aligned with our culture.

This policy presently is presently incomplete, and is a work in progress. If you have feedback, please [make your voice heard](#get-in-touch)!

[TOC]

## Privacy

This is a semi-private server. A member may request an invite for a friend who they think would be a good cultural fit. Said another way, we have no strong approval system for new members, and you ought to keep this in mind.

We cannot guarantee that the content posted to the server is ever held in complete confidence, but we do strongly encourage being considerate and reasonable when dealing with what you learn about our members.

For details on how our services and system operators make use of your data, see our [Privacy Policy](../user-help/privacy.md).

### Confidentiality

All _sensitive_ content posted to the server and expressed between our users in DM are to be held in confidence by default. Examples of such content includes, but is _not_ limited to: opinions, experiences, secrets, medical or psychological disclosures, relationship dynamic specifics, and photography featuring member(s).

It is expected that every member use their best judgment when considering sharing conversation fragments off-server; in many circumstances this is likely acceptable as long as it is anonymized. Media featuring explicit content of a server member is never to be shared elsewhere without the express permission of all parties featured.

### Direct messages

If you don't want to receive DMs for any reason, you have the option to disable that in your client:

* On Desktop, while in a server channel, on the sidebar, click the server name above the channel list, then Privacy Settings to expose a toggle
* On Mobile, while in a server channel, side open the sidebar, then tap the server name to expose a toggle

We also offer [self-assignable](../user-help/roles.md#direct-message-willingness) DM preference roles. You may be interested in electing `DMs closed` to advertise that preference openly to other members.

*If you were harassed*, or received unwelcome or alarming messages from another user, _please_ do not hesitate to [reach out to a staff member](#reporting-problems)!

### User logging

Members shall not be granted, by any staff member, consent to run a client, self-bot, network interception apparatus, or otherwise some facility that logs aria chat service message contents without it meeting the criteria specified in our [Operations](../staff-help/operations.md) policy, unless it is the official Discord client.

A member running such a non-standard logging facility must notify staff of it upon joining and reading these guidelines. It is expected that they participate in a discussion with us to verify that it does indeed meet all required criteria. Sanctions may be enacted should staff become aware of an undisclosed logging facility that a member is running.

## Content & Conduct

This community was founded for the expression of sexual and kinky concepts and the exploration of self that so often results. You _will_ encounter niche porn, exhibitionism, erotic roleplaying, and hard kinks such as CNC (consensual non-consent) or snuff.

### Be kind, or at least civil

Consider the platinum rule: **treat others as they wish to be treated**

People can be vulnerable when discussing highly personal matters such as sexuality. This is often doubly so when the practice of kink is involved. We ask that all users do their part in being conscientious and practicing empathy, especially when discussing sensitive and serious matters. Banter and friendly ribbing is welcome, but is not always appropriate given conversational context.

We expect users to be civil and considerate when critiquing others' posts or requesting accommodation. Similarly, when the need arises to express offense or hurt over another users' words, try to be clear as possible with your phrasing as to avoid misunderstanding of your mental state and the behavior change being asked after.

### Be open, or at least tolerant

Many of our users have suffered bullying, abuse, or ostracization growing up. If you're unable to tolerate seeing a controversial or "unsavory" kink put on display and actively enjoyed, this might not be the place for you. Do not kink-shame in an unfunny way. This is not license to disguise prejudice or resentment with thinly veiled humor.

### Assert your needs, and care for yourself

We do not enforce a policy requiring spoilering nor the inclusion of content warnings for "offensive" or potentially emotionally-activating content. It is _certain_ that you will encounter posts involving trauma and abuse in the form of media, discussion, emotional processing and even kink. Please, take care of your own mental health, first and foremost.

With respect to conversational topics found to consistently elicit strong, negative, unwanted emotional responses, we recommend vigilance and self-policing such as to minimize exposure. It is usually possible to determine what a conversation is about before getting too deep into the details. These tactics are especially endorsed for those who find themselves becoming inconsolable and combative with others when activated.

In the case of the media sharing channel(s), you might find it helpful to disable images and embed previews in your client. Alternatively, a [bookmarklet](../user-help/tools.md#toggle-images-in-discord) is provided that allows one to toggle image display in the web client.

We realise these aren't fine-grained nor ideal tools for managing your exposure to unwanted media. If you have any suggestions as to how we might be able to provide better accommodation without revising the content policy, we're all ears!

### Play in an informed and safe manner

Participation in the ERP (erotic roleplay) zone is contingent on being willing and able to abide by sound consent and kink safety norms.

You must be able to assert boundaries where necessary and prudent for your own protection, and respect the boundaries and signales (such as safe words) expressed by others.

It is highly recommended to do independent research or consult others in `#peer-counseling` on the topic of safety surrounding the practice of a kink that you are new to (see [Education and support](#education-and-support)). This due diligence is the responsibility of _everyone_ involved in the play and not _just_ the dominant/top.

Staff are willing and able to guide, mentor, and correct in the case of harmless mistakes made, but repeated demonstrations of poor safety will lead to warnings, and potentially sanctions due to perceived negligence.

## Education and support

Part of our mission as a community is to foster an intimate, comfortable environment that facilitates learning and support. Many of us are friends, and if not, at least desire to share a friendly space that facilities growth. To that end, we host several channels for working through the complexities of relationship dynamics and mental health issues, whether intersecting with kink or not.

### Peer counseling

As some topics appearing in `#peer-counseling` could be highly sensitive in nature, we strongly encourage the exercise of caution and forethought. This is not a role-gated channel. Consider if you _want_ everyone in the server to be able to read about what you want to say.

That's not to say that you _can't_ express your raw emotions there; volatility is unexpected and understood to come with the territory. We do ask that members consider limiting the scope, when possible. This could be done by inviting people who want to help into a private thread after soliciting the channel with a brief description of the topic.

### Mentorship opportunities

We encourage our experienced users to offer mentorship to our less experienced, but highly motivated members who are interested in learning about kink.

**For mentors** — you don't have to have 10 years of experience in your local kink scene to be qualified for this! Think of it like a sort of buddy system to help guide someone through the challenges inherent to being naive yet enthusiastic about a potentially risky activity.

**For students** — even if you _do_ feel you're broadly experienced, all it takes a strong desire in another's breadth of knowledge! Have someone in mind? Ask if they're open to regularly talking about it!

We strongly recommend the use of private threads for these arrangements in the case of needing to pull in staff or other members, as necessary.

### Contributing

Want a resources added to the `#library`? Contact any `@staff` member, or post in `#patron-services`!

Want to infodump or host a seminar on a topic to a wider audience? Or maybe schedule some time to coach a few interested people through the basics of a skill? Advertise your event in `#peer-counseling`. Staff can help you coordinate!

## Trust & Safety

It is the responsibility of administrators and moderators to sustain the harmony of the overall chat zone. We do our best to mediate conflict and cultivate a comfortable environment for users to explore and express themselves.

The process by which staff conduct and comport themselves on our chat services is extensively, but non-exhaustively documented in our [Moderation](moderation.md) policy. The distribution of responsibilities between staff roles is laid out along with an enumeration of teammates on the [Staff](../user-help/staff.md) page.

### Get in touch

We're here to serve you. Staff always welcome DMs from members to discuss server-related matters — conduct or content reports, propositions, bot feature requests or bugs, etc.

If you ever see something you don't like happening on the server, or otherwise want to call attention to a post you've observed that is felt to be an issue, you have options:

- For protracted conflict occurring in an active conversation that needs immediate attention, ping \@staff.
- To report the conduct of a user, DM any staff member — \@steward, \@council, or \@advisor
- Reacting with :eye_in_speech_bubble: to a post reports it anonymously to staff, in private.

We intend to add advanced, anonymous messaging to our bot that would allow private and public conversations to be had with staff and other users soon. To fill this void in the meantime, you may message any administrator personally on [Session](https://erin.lv/go/session), an anonymous, secure, cross-platform messaging app ([getsession.org](https://getsession.org)).

### Community engagement

A common, critical aspect of all staff is mediation and enforcement. When interacting with members, you may see the tonal indicator `[mv]` used. This jargon (short for "mod voice") implies that they are acting in official capacity. This doesn't translate to issuance of a warning, and can instead merely be a note that they wanted taken seriously.
