# Matrix

This document describes rules, guidelines and expectations specific to our [Matrix](https://href.li/?https://matrix.org) chat service. For introductory usage information, see the [user help](../user-help/matrix.md) page.

- Acceptable use
    - [Underage content](#underage-content)
- [Comfort and partitioning](#comfort-and-partitioning)

## Acceptable use

Apart from the particulars details below, we moderate the server according to our [Guidelines](guidelines.md) and [Moderation](moderation.md) policies.

### Underage content

We've selected a hosting provider in Hungary to afford additional rights to our users beyond what is permitted by Discord.

Specifically, we permit the sharing of of lewd illustrations of fictional, underage characters so long as it is _not_ photorealistic. The law explicitly supports this distinction.

Photography or photorealistic reproductions of real-life children depicted sexually is forbidden by law. Fictional, underage characters depicted sexually and rendered in a photorealistic manner through computer graphics are forbidden.

It would be unwise to admit engaging in sexual acts with minors when circumstances point to a violation of local law. While there may be age couplings that are illegal yet ethical in the eyes of our community, the risk of abuse cannot be overlooked. We will investigate members who admit to lewd activities with a minor, regardless of ages at the time. If the member is uncooperative, we deem their conduct abusive, or the age of the minor was unjustifiably low, they will be banned across all chat services, at a minimum.

None of the above is to be construed as endorsement of an adult engaging sexually with minors in real life, or online. It is the opinion of our staff that this is a bad idea, even if it were theoretically possible for it to be carried out in an ethical, mutually consensual, and non-harmful way.

## Comfort and partitioning

Expect to find discussions of ethics and stigma surrounding consumption of drawn underage illustrations, child pornography, ageplay, and attraction to minors. If you aren't comfortable with this, but want to make use of the service, please [let us know](guidelines.md#get-in-touch) and we will try to come up with some suitable accommodation.

We are presently quarantining media posts by channel partition. All explicit media is to be relegated to `#gallery`. If you are uncomfortable or otherwise do not want to see the sort of media mentioned above, simply do not request access to this channel.

Further, the Element client has the ability to disable image previews:

1. Tap the ⚙ icon
2. Tap 'All Settings'
3. Select 'Preferences' from the left-hand tree
4. Scroll down to the 'Images, GIFs and videos' subsection
5. Toggle off 'Enable inline URL previews by default'
6. Toggle off 'Show previews/thumbnails for images'
7. Close the settings modal
8. For each channel, right click and select 'Settings'
9. Scroll down to the 'Other' section
10. Ensure 'Enable URL previews for this room (only affects you)' is toggled off
