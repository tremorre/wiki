# Sanctions

This document describes how roles related to user sanctions are designed, and how staff are to go about applying them when enforcing the [Moderation Process](../policy/moderation.md).

[TOC]

## Discord-native features

### Timeouts

Global moderators (`advisor`) and higher staff classes may put members in time out.

The affected user won't be able to send messages, add reactions or join voice channels for the duration specified.

This feature is accessible via long-pressing or right clicking any member's profile picture and then selecting 'Timeout.' A duration and reason must then be given.

The available durations are not precisely granular i.e. 1, 5 or 10 minutes, 1 hour, 1 day, or 1 week. The reason is presented only in the server audit log and not to the member.

See Discord's [Time Out FAQ](https://support.discord.com/hc/en-us/articles/4413305239191-Time-Out-FAQ) for a step-by-step guide with screenshots.

### Bans

Only administrators (`council`) are functionally permitted to ban members.

Bans are **not** to be placed frivolously. Staff **must** follow [proper protocols](../policy/moderation.md#bans) prior to issuing one.

The affected user will immediately be kicked from the user, and will not be able to rejoin it by invite. An IP ban is also placed, under the hood, in an effort to mitigate ban evasion.

This feature is accessible via long-pressing or right clicking any member's profile picture and then selecting 'Ban.' A reason must be given. **Never** opt for any of the member's messages to be deleted!

The reason is presented only in the server audit log. The member will not be notified of the reason on ban placement, nor if they attempt to rejoin the server via invite.

## Bot commands

### Activity Watch

[Watches](https://gitlab.com/aria.theater/cables/-/issues/67) notify staff of a given member's activity every 5 minutes that they are active. You may manage who is subject to this observation with cables. Only issue these commands in staff-only spaces, such as `#janitorium` or `#laboratory`.

List all subjects:

```
.watch show
```

Start reporting on a member:

```
.watch add @member
```

Stop reporting on a member:

```
.watch del @member
```

## Roles

The management of the member's roles to achieve a given restriction must presently be handled manually. The commands described in this document do _not_ presently function! See the `cables` [issue tracker](https://gitlab.com/aria.theater/cables/-/issues) to track their implementation progress.

**For any sanctioning role to be applied correctly**, all other gating roles must be removed, or else the member will continue to be able to post in those channels. This _includes_ `confidant` too!

### Detention

The `ponderous` role confines a member's posting ability to a single, pre-existing, staff-only channel as a means of immediately restricting their activity in emergency situations.

Specifically,

- Makes most channels read-only
- Hides voice chat channels
- Hides `#registration`
- Send allowed only to `#timeout`

(*Note*: `#registration` is hidden since Discord permissions do not distinguish between contributing a react when a message already has one on it versus adding a new react.)

### Mute

This role is presently unimplemented. It would behave exactly like a time out, but not be subject to a pre-defined time limit.

### Exile

This role is a near-ban of the member. It prevents them from viewing the majority of channels on the server, and they are confined to read-only access to `#tour`, `#bulletins` and `#patron-services`.

We use this when we feel the member would not pose a significant safety risk if they were to have access to the user list of the server. As such, the primary reason to employ an exile rather than a Discord-native ban would be to allow other members to find and contact the banned member through the user list.
