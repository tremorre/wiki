# Moderation Process

[TOC]

## Observation

It is the responsibility of all staff to read the posts, click the links, watch the videos and expand the thumbnails. We understand that people generally have lives, and that they may fall behind at times, and that's okay. We also understand that you may need to skip certain conversations and media due to trauma.

We ask all staff enable [thread auto-subscription](../user-help/bots.md#thread-auto-subscription) through Fletcher. This will help you be made aware of new threads as they are created by adding you to them, allowing them to appear in your channel list with no manual intervention on your part.

Skimming or preferentially ignoring a certain user's posts is discouraged. If you are finding it difficult at any time to stay interested in reading a conversation or facing a discouragingly hefty backlog, consider soliciting other staff as to their own backlog. You may find that someone else is caught up, and be able to take a break or mark everything as read, if necessary.

If you find your duties becoming onerous, frequently, we encourage you consider an extended break to recuperate your enthusiasm. Moderation is typically a thankless job, and burn out is real. Take care of yourself first and foremost.

## Bias

Chat moderators are encouraged to recuse themselves from mediating conflicts between users where one or more of the parties has a significant relationship(s) with the moderator. A significant relationship is any relation perceived by a moderator or a plurality of users to dis/advantage one party with respect to another in a moderation decision.

Another scenario would be where the moderator themselves become emotionally compromised. If said moderator is unable to detect this themselves, other online staff ought to advise them to step away and leave the matter to someone else.

## Mediation

When we read the chats and see user conduct issues, the first course of action involves talking to the user(s). This does not necessarily rise to the level of a warning and isn't _bad_. These notes are generally for educating people or letting them know about specific boundaries. If we observe a pattern of giving notes to a specific user, this may prompt us to mediate or apply sanctions.

### Conflict

1.  If there is an active conflict happening in-server, moderators should de-escalate by using mod voice (detonated by `[mv]` prefixing your post) to tell the engaged parties to stop, and take a step back from the conversation. If this is ignored, the moderator is to issue warnings, and potentially detention as necessary.
2.  Afterwards, moderators should contact the involved parties individually via modchat channels to understand the root cause of the conflict and have everyone's perspective.
3.  Moderators can brainstorm solutions in internal mod channels for potential remediation. Potential remediation includes asking users to block one another, clarifying a misunderstanding that may have occurred, suggesting a heuristic update for engaging with one another, or suggesting additional boundaries.
4.  Get feedback and agreement from involved parties and stated commitment to abide by any agreed-upon terms.

### Mental Health

Our users, like many living in the modern world, encounter challenges with their mental health that bleeds into how they interact with others on the server, whether related to kink, or not. Part of a moderator's job is to consider how these intricacies may be influencing the chat environment or, occurrences of play the user describes or engages in on-server. Similarly, this can manifest in reverse, with interactions on the server impacting a member's mental health.

Examples:

*   Emotional lability
*   Poor self-advocacy
*   Self-harm behaviors
*   Patterns of risky sex
*   Changes in self-conception
*   Isolation from offline friends

Interventions:

*   Encouragement to disengage from a conversation (modvoice optional)
*   Encouragement take a break from posting for awhile to take care of themselves, perform self-care, etc.
*   Encouragement to use or redirection to `#peer-counseling`
*   Opening a modchat channel to air concerns and give advice
*   Discouragement from posting about their desire to conduct self-harm in inappropriate channels such as `#theatre` or `#rehearsal`
*   Redirection of self-harm thoughts to minimize the chance of them being carried out
*   Offering advice on safer sex practices, such as barriers, and PrEP, or STI testing resources
*   Exploring the user's thoughts on how their identity or experiences may be changing
*   Encouraging self-care or otherwise treatment for pre-existing or emerging behaviors considered to be unhealthy for the individual

While our demographic holds values for expressions of vulnerability and empathy in the interest of emotional support, moderators and friends are not a replacement for professional resources, and we are not credentialed in the treatment of mental health. It is not the moderation team's duty to provide limitless or even consistent assistance with recurring and resistant manifestations of mental health problems in a single member.

When a moderator's skills are lacking, and support appears to be inadequately being provided to a member, it is appropriate to encourage them to seek our, or refer them to professional services. It ought to be made clear that in doing so, we are not necessarily discouraging them from discussing their mental health on-server, as long as it is appropriate and done in proper channels, such as `#peer-counseling`.

In times of crisis, such as plausible suicidality or dangerous drug use, the moderator ought to first inform senior staff of the matter. They then should proceed to [opening a modchat channel](../staff-help/channels.md#modchat) with the member, and refer them to a crisis support line. If the user is unresponsive, escalate to a direct message, and informing any known close friends, if possible.

After an incident involving significant mental health issues occurs resulting in moderator intervention, make sure to follow up with the user experiencing the trouble within a few days to check in on them. Ask them for their perspective on the incident, and if appropriate, if they themselves followed up on any advice given.

Typically, a member ought not be removed from the server for mental health issues. If their chat dominates `#peer-counseling` or other areas of the server despite multiple redirection attempts, a better course of action may be to encourage them to create their own server and invite parties who regularly engaged with them on the topic. There may be exceptions to this if the comfort and safety of many other members change with the state of the person suffering. See the [Sanctions/Bans](#bans) section for general information on considering such actions.

## Consent violations

Procedure for handling consent violations varies by the context in which the violation occurs, but always serves to mitigate risk of harm immediately (if applicable) and follow up to prevent such violations in the future.

Violations of consent that occur **off server**, whether in DM's, other kink communities, or in IRL practice, will be investigated to the best of the staff's capabilities, so as to inform the most effective course of action to ensure the safety of the userbase on aria.

Violations of consent that occur **on server,** **in dungeon scenes**, will be responded to immediately by the dungeon monitor to limit the scope of damage. The response will be proportional to the severity of the violation. This may range from a verbal reminder of limits or agreements, up to termination of the scene, detention and a conduct investigation.

Violations of consent that occur **on server, in textual ERP** between two or more members will be responded to as immediately as is feasible, if another user doesn't step in for (self-)advocacy. Typically, if the violation is minor, and the user subject to the transgression is forgiving, our response will be only a note / reminder of limits and preferences, regardless of incident count. However, if the user frequently upsets the same user, or there is a pattern of them upsetting many different members they play with, our response may vary from warnings, detention, or mutes. Such a pattern of concerning behavior would also be worthy of an activity watch being enabled.

Violations of consent by **admittance, log relay, or the sharing of content** may require an investigation, depending on context. The words 'rape' and 'force' are often used as slang for consensual play, and should not be confused with admittance of wrongdoing. However, it is important for staff to perform due diligence in seeking clarification from the user and any others who may be mentioned in associated content. There is also an expectation for members to consider aspects of consent and safety when making posts like this. Repeated failure to do so in channels such as `#theatre` and `#rehearsal` would justify warnings and counseling. Response is expected to be prompt, resources permitting.

**Exceptions** from warnings and punitive actions may exist depending on the context of the consent violation. As many members struggle with trauma reactions and various other mental health challenges, two personalities may be prone to conflict for reasons not entirely under anyone's control. As such, it would be unreasonable to assign fault to either party. Staff are encouraged to attempt mediation and conflict mitigation. Unfortunately, it may be necessary to recommend one user blocks another if there are irreconcilable personality differences.

## Content removal

Pursuant to the enforcement of Discord's [terms of service](https://href.li/?https://discord.com/terms) and [community guidelines](https://href.li/?https://discord.com/guidelines), staff are required to remove content from the server that is explicitly forbidden:

*   Sexualized depictions of minors (fictional or otherwise), including characters who are canonically underage but who appear older and canonically old characters who appear underage
*   Sexually explicit content of other people without their consent
*   Real-life gore, excessive violence, or animal harm
*   Content that glorifies or promotes suicide or self-harm

While we disagree with many points listed in Discord's service agreements, there is only so much we can accommodate without facing unacceptable risk to the continued operation of the server, and our administrator's accounts.

Textual posts or links to stories involving the sexualization of fictional minors are exempted from the above. Our reasoning for this exemption is that Discord's highest priority is protecting people, and not hosting what may be considered child pornography on their content delivery network.

While we must delete content according to the above policy, posts of this nature never warrant escalating to warning or sanctions. It is expected of users not to abuse this, and to make their best effort in avoiding posting this sort of content on Discord. If you find a user frequently posting this sort of content, a modchat channel may be warranted to help them discern youthful traits such that they may better avoid posting such characters in the future. You may also refer them to our Matrix server, where such content is permissible as long as it is not photorealistic or actual real-life depictions of children.

## Sanctions

### Warnings

Effect: Staff informs user that the user has been warned, the specific behavior prompting the warning, applicable rule violation(s), and a note on how to avoid this behavior in the future. Warnings are tracked in internal mod channels to inform future moderation decisions.

Scenarios:

*   User has already gotten multiple notices regarding the same rule violation, e.g. requiring staff to remove content in violation of discord ToS several times in recent memory.
*   User is promoting or engaging in hostile discussion without respecting civil discourse norms.

Use bot command in reply to the relevant post:

```
.warn @tag "reason, relevant rules, advice"
```

### Detention & Mutes

Effect: Uses cables detention feature (or manually adjust user roles) to confine their write access only to a modchat channel, leaving only read-only to some other channels.

Scenarios:

*   For conflict de-escalation: apply for less than a day to allow participant to cool off after they have continued engaging despite staff issuing a warning, e.g. 1-2 hours.
*   After persistent disruption and/or receiving multiple sanctions from staff: apply for prolonged period of time, e.g. 1-4 weeks.
*   After observation of a significant consent violation in a dungeon scene in which it is critical that the user in question be removed from the presence of the playmate and other users while counseling of all parties is carried out

### Bans

Considerations:

*   How long the user has been a member of the community
*   Their standing amongst users and how it may differ in recent past compared to distant past
*   The number of and significance of reports from other users or internally by staff against their posts or activities off-server

Prerequisites:

*   Any administrator may enact a Discord-native ban, or apply the exiled role in case of emergencies. These are subject to review by the council, or steward.
*   Examples of emergency situations would include: spamming, doxxing, or tangible threats.
*   For indefinite bans, unanimous consensus of the council must be attained.
*   For bans involving on-server conduct issues, at least one warning must have been issued prior.
*   Any ban issued for alleged off server conduct will be investigated within reason and to the extent our limited ability and include interviews of all accessible parties claimed to be involved, and relevant first-degree connections.

Effect:

*   A message is sent by an administrator to the user in question with the reason for their ban.
*   User is kicked from the server and banned from rejoining using Discord native functionality.
*   Their active invites, if any, are revoked.
*   At least one administrator, including that which sent the notice, sends a friend request to them in case they would like to appeal.

Justifications:

*   User has not been responsive to previous attempts at communication; doesn't show willingness to change problematic behavior, self-learn kink safety, or respect others' boundaries; and staff has good reason to believe their continued presence, even with read-only privileges, would threaten the comfort and safety of members in the community.
*   User is not a good cultural fit and evokes widespread dislike from the userbase for e.g. lack of civility, few meaningful and appreciated contributions, a consistent failure to show empathy, being transphobic in an unfunny way, etc.
*   User has been found to be distributing sensitive logs or personal lewds of other members to public websites en masse without the explicit consent of the user(s) in question, and in fact counter to their desire.

### Appeals

If a user is outright banned, or otherwise sanctioned to remove permissions to some aspect of the server, they may appeal this action to administration.

The consideration of an appeal is only guaranteed for the first appeal of a given administrative decision; further appeals to the same decision may be considered at staff's discretion. Second appeals of a ban may be more likely to be granted consideration given time.

In their appeal request, they must state their case for why they believe judgment is in error, or request a pathway from us that would alleviate our concerns.

Deliberation of either case should take no longer than a week, and may involve:

*   an internal investigation, including solicitation of the userbase
*   hearing appeals from friends of the user who may or may not be on server
*   discussions amongst staff

After this has concluded, an update must be provided to the user in question.

To form an improvement plan, administrators ought to consider:

*   Giving advice specific to the reasons that the user was sanctioned that would lead to their behavior being acceptable in the future if adhered to
*   Providing external resources, such as literature or support groups for the user to learn from
*   Who the user has in their own social circles (that is not an aria administrator) to rely on to help them make these changes, if anyone
*   If the user is willing and able to adhere to the plan, by asking them directly

If there was a safety concern, sanctions may be considered as part of their terms for re-admittance, such as:

*   Agreement to conducting play on-server _only_
*   Declining any dungeon requests unless a senior staff member is available to act as dungeon monitor
*   Requesting that staff proofread "spicy" posts involving edge play before making them public, for the comfort of other users

### Probation

If a user is granted their appeal and allowed re-entry to the server after a ban, they will be subject to a probationary period where their posts are closely observed to track if they are meeting the terms they agreed to.

This may involve placing a watch on their activity, effectively notifying staff in near real-time. A reason to do this would be if they were banned for particularly concerning expressions of kink or, consent violations.

```
.watch add @tag
```

## Dungeon monitoring

### Selection considerations

*   bias such as friendly relations between the monitor and all participants
*   expression of moderator time constraints to the event organizer to allow them to plan their scene accordingly
*   the organizer has either established limits, and preferences adequately, or has a post in `#moodboard` expressing the same

### Organizational duties

*   help plan, promote and otherwise organize the scene if it is requested far in advance, and the requester desires this assistance
*   create a scheduled event for 🔉`dungeon` with times and terse details of the scene
*   create a thread in `#theatre` when appropriate; if the requester wanted us to run promotion, do this three days in advance of the event or after planning is completed, whichever comes first
*   create and pin the first post in the thread, including:
    *   the original request details
    *   a link to the scheduled event
    *   any details elucidated during planning
    *   user signals and limits
    *   a link to the performer's `#moodboard`
    *   a ping to confidant, if the scene is public, and it was requested
*   immediately or shortly before the scene starts, verify/assign participant and/or spectator roles required for access according to the scene spec
*   when the scene/event starts, and if users join late, post a message link to the pinned post so that people may refresh themselves
*   when the scene/event starts, if the requester wished it so, ping confidant

### Interpersonal responsibilities

*   mediating any conflicts that might arise
*   insure that users comply appropriately with the performer's signals, be they uttered vocally, gestured visually or expressed through text
*   request microphone adjustment if anyone, but especially the performer goes silent for a significant duration, or it is evident they are speaking but cannot be heard
*   if a consent violation or unacceptably risky act takes place, end the scene by stepping in to voice the issue and revoking roles if necessary
*   in in the event that you cannot stay for the duration of the scene, duties ought to be attempted to be transferred to another moderator. If no mutually agreed upon replacement is available, end the scene by making your time limitation, preferably with at least 10 minute advance notice to participants such that they may wind down and conduct aftercare
