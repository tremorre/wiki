# Scenes

- [Scenes](#scenes)
  - [Composition](#composition)
  - [Planning](#planning)
  - [Coordination](#coordination)
    - [Requirements](#requirements)
    - [Optional details](#optional-details)

## Composition

* A voice or video-based exhibitionist event taking place in :sound: `dungeon` (public) or :sound: `dungeon-priv` (private)
* No additional participation on mic or video is required outside of the host's planned exposition of lewd or kinky acts
* Participants are defined as additional players who will be taking an active role on voice to help conduct the scene
* Spectators and commentators are able to connect, view and listen to the broadcast. Those who wish can post in the scene thread and, if appropriate, may even be encouraged to offer directives to the host
* Organization of logistics, and in-scene monitoring will be carried out by a ranking staff member

## Planning

* Organizer and any participant interest must be declared such that an available staff member is clearly made aware of the proposed event
* We generally require events to be reserved ahead of time, though exceptions may be made on a case by case basis depending on staff availability, details of the event, player familiarity, experience level, etc.
* Ad-hoc use of the dungeon is not a right, nor can it be expected that such a request will ever be granted, no matter the details. As requests must be reviewed, coordinated and manually approved by staff, it is simply infeasible for us to afford assurances of same-day booking
* Tentative requests may be lodged many days in advance. Confirmation must be given at least 24 hour prior in these cases, but ideally at least 72 hours prior to help garner user interest

## Coordination

Requests are to be made by creating a private thread in `#playwriting` with information meeting the requirements listed below, and a `@staff` ping to draw moderators in

### Requirements

* Whether or not the scene is invite-only
* For public scenes, a list of participants who will be speaking/broadcasting
* For private scenes, a list of invitees
* Time of event, and anticipated duration
* Established signales (the traffic light system is commonly known, and a safe bet,  but you can go custom if you like)
* At least, a rough sketch of planned activities

### Optional details

* Scene promotion; if you'd role ping in advance and at time of event
* Designation of who will perform aftercare, and/or what you find helpful for aftercare
* A detailed list of desirable activities that participants can draw from for this particular scene (separate from your `#moodboard` thread, or an external kink doc)
* Preferred moderator(s)
