# Operations

This document describes internal policy for managing data and designing systems that aria members make use of.

[TOC]

## Confidential user data

When considering copies of user-generated data, or user-correlated metadata, it:

* must be encrypted at rest on a medium controlled by `sysop` or `steward`
* should be subject to a short retention policy of less than or equal to a week, when possible
* should be anonymized where possible, if such a choice doesn't hinder core functionality
* must never be transmitted in bulk to parties that are not intimately involved in operations or administration

## Encryption

Sound cryptography standards ought to be considered and leveraged wherever appropriate given the use case.

### Tokens and keys

Server-side encryption keys and user API tokens,

* must be minimally stored encrypted at rest
* shall not be intentionally transmitted to unauthorized parties
* must be encrypted in transit if distributed to authorized parties

Server-side encryption keys should optimally not reside on the same physical machine that they protect. For example, AWS KMS may be leveraged to remotely store and afford secure programmatic access to cryptographic operations.

### Minimum standards of resiliency

Modern cryptography must be employed to a minimum standard as recommended by cryptography professionals; adequate sources include,

- highly accepted Internet postings
- IETF RFCs classified as best current practices, proposed standards, or active drafts

For example,

- [SafeCurves](https://safecurves.cr.yp.to/)
- stribika's [Securing Secure Shell](https://stribika.github.io/2015/01/04/secure-secure-shell.html)
- [RFC 7696](https://www.rfc-editor.org/info/rfc7696): Guidelines for Cryptographic Algorithm Agility and Selecting Mandatory-to-Implement Algorithms
- [RFC 8731](https://www.rfc-editor.org/info/rfc8731): SSH Key Exchange Method Using Curve25519 and Curve448
- [RFC 7525](https://www.rfc-editor.org/info/rfc7525): Recommendations for Secure Use of TLS DTLS
- [RFC 9142](https://www.rfc-editor.org/info/rfc9142): KEX Method Updates and Recommendations for SSH
- [RFC 6234](https://www.rfc-editor.org/info/rfc6234): US Secure Hash Algorithms (SHA and SHA-based HMAC and HKDF)
- [RFC 6151](https://www.rfc-editor.org/info/rfc6151): Updated Security Considerations for the MD5 and the HMAC-MD5 Algorithms

When selecting a subset of available algorithms that a client or server would offer to negotiate, favor the most resilient option such as strong elliptic curves over RSA whenever available. If no algorithms are available that are mentioned in proposed standards or active draft RFCs, consider upgrading the library implementing cryptographic primitives.

At design phase, research must be conducted each time cryptography selection is implicated. If different outcomes are found since the last survey was performed for other software still running in production and serving members, tickets must be lodged to update said software.

### Exceptions

Encryption may not be necessary considering the networks involved in application traffic patterns or data passed over the wire. For example, while an API token may be exposed over an HTTP VNC connection carried over the internet, if this "secret" isn't critical to service operation nor granting of access to confidential user data, such an exposure may be acceptable.

## Patching

Computer systems and software with external dependencies must be periodically audited for security vulnerabilities.

For **major** classifications, patching is **strongly recommended** to occur in a timely manner. Exceptions to patching would include infeasibility due to impending obsolescence and replacement, or the effort to upgrade dependent libraries in the software being unreasonably high. In either exception cases, a plan to mitigate the vulnerability with a method other than patching must be crafted in a timely manner, including publication of a reasonable timeline to implementation.

For **critical** classifications, patching **must** occur in a timely manner.

## Security incidents

Should compromise of sensitive information take place,

* an internal investigation must be launched to identify the root cause
* systems must be patched such that the exact vector of attack upon the service in question will never recur in the future
* if encryption keys are implicated, they must be rotated at the server or service level
* disclosure to the userbase of the type of data, scope, and impact period must occur within 7 days of the incident becoming known, regardless of whether or not the investigation has concluded or remediation has occurred

## Controlled access

Best practices for authentication, authorization and auditing ought to be considered across operational and development domains.

### Application design

Public endpoints exposing command and control mechanisms or exposing confidential data must be authenticated, and should be authorized and audited.

When auditing is implemented, IP addresses should not be logged indefinitely, and ideally, not at all.

### Unattended devices

Users with privileged access such as administrators, and system operators who store sensitive data, encryption keys, or authorized means of accessing the aforementioned must implement the following practices on implicated personal devices:

* Commitment to manually lock the active session before leaving the device unattended e.g. walking away from it
* Configure automatic session locking to ensure the above in cases of forgetfulness

### Multi-factor authentication

Administrators with access to service accounts must configure MFA, where available. For example, aria Discord server administrators must configure MFA, and this is encouraged through a service-side requirement that it be so before allowing certain privileged operations to be carried out.

It is strongly encouraged that OTP generation not occur on the same device they are signed in in from. [Yubico](https://yubico.com) sells affordable hardware tokens that may be considered.

## Open source software

The use of open source code in project development for any facility being considered for integration into chat services is strongly recommended.

If this is not feasible,

* the service's risk to community operations and user privacy must be thoroughly analyzed
* permissions granted to the token must be on an as-needed basis
* the idea must at least go through internal review of our [Governance](../policy/governance.md) process, and will be subject to the steward's executive veto power
