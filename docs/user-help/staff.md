# Staff Roster

This document enumerates the staff members of our community.

For details on how to contact us, see the [Guidelines](../policy/guidelines.md#get-in-touch).

[TOC]

## Server Owner

`steward` and 🔑 — Issues executive edicts, performs overall community steering, plus all duties of lower roles

* erin (`erin#5201`) — Founder and former primary moderator of the thirsty channels of ACN. 2+ years in her local kink scene. 8+ years practicing kink hands on. 8+ years of online leadership and moderation experience
* aria (`aria#8744`) — An alt of erin's that functions as a a proxy for holding ownership of the server for opsec reasons

## Administrators

`council` — Reviews community proposals, functions as dungeon monitor, and carries out both soft & hard moderation

* Eli (`eli*#0001`) — A doggo who cares about thirsty chats and likes to help! 1 year in his local RACK community. 3+ years practicing kink in his local scene and privately
* Thorin (`Thorin H.#7978`)  — Designated People Person and friend to just about everyone, hosts the less kink-centric social engagements of the server such as game and (eventually) movie nights

You may also contact any administrator personally on [Session](https://erin.lv/go/session), an anonymous, secure, cross-platform messaging app ([getsession.org](https://getsession.org)).

## System Operators

`sysop` — Manages community technology projects, contributing to the maintenance and operations of servers and services

* Eli (`eli*#0001`) — Wrote, works on improvements for, and primarily maintains the codebase of one of our Discord bots, @cables. he also assists with non-Discord server operations
* erin (`erin#5201`) — Contributes bot improvements to cables, charts server statistics, maintains ancillary chat technology, contributes server propositions for additional chatware, and has other integrations in the works!

## Moderators

`advisor` — Acts as dungeon monitor, and advises users and staff on matters of conduct

* nyuu (`nyuu#5711`) — Veteran of the thirst wars; knows 30-50 things about safe kinking
* Cassandra (`UncertainKitten#6671`)  — Maker of many mistakes so you don't have to.  Over a decade of experience with practical mad psychology and its intersection with kink
