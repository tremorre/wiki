# wiki.aria.theater

 [![pipeline status](https://gitlab.com/aria.theater/wiki/badges/main/pipeline.svg)](https://gitlab.com/aria.theater/wiki/-/commits/deploy)

Built with [mkdocs](https://www.mkdocs.org/) using the [gitbook](https://gitlab.com/lramage/mkdocs-gitbook-theme) theme.

The [`PyMdown extensions`](https://facelessuser.github.io/pymdown-extensions/) Python module is leveraged to support emoji shortcodes and escaping arbitrary characters (e.g. to avoid \@'s turning into usernames on gitlab).

# Requirements

Python 3.8 or something, I think.

```bash
virtualenv venv && source venv/bin/activate
pip install -r requirements.txt
```

Dockerfile Soon:tm:

# Development

You can use the `mkdocs serve` command to stand up a small local webserver that supports live reloading.

If you'd prefer not to work locally, `mkdocs` tests will run on every commit to `main` or feature branch. Project members can view CI/CD jobs to see lint output.

# Contributing

As this is a community wiki, only members of our chat services are eligible to contribute.

1. Sign up for a Gitlab account
2. Request access to the project
3. Clone repository
4. Create a new branch
5. Commit your proposed changes
6. Submit a merge request to `main`
7. Solicit review from staff or other community members in `#patron-services`
8. Integrate any feedback
9. Await reviewer acceptance
10. Squash and merge

# Deployment

The `deploy` branch's build artifacts integrate with Gitlab Pages, which serve [wiki.aria.theater](https://wiki.aria.theater)

Merge requests ought to be between `main` and `deploy` only, and a staff member marked as the reviewer.
