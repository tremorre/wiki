# Privacy

[TOC]

## Data use practices

System operators run several applications that access and store your data outside of the Discord service.

We do this for several reasons,

* to offer enhancements to the Discord service through integrations such as bots
* to generate statistical information for internal use
* to preserve the integrity of chat logs in the event of emergency situations impacting the service

We make use of several third parties to store your data, including

* [Discord](https://href.li/?https://discord.com/privacy), who leverages [Google Cloud Platform](https://href.li/?https://cloud.google.com/terms/cloud-privacy-notice), [CloudFlare](https://href.li/?https://www.cloudflare.com/privacypolicy/), and probably others
* [Amazon Web Services](https://href.li/?https://aws.amazon.com/privacy/), for bot hosting and log storage
* [MikroVPS](https://href.li/?https://www.mikrovps.net/en/about-us/privacy-policy), for Matrix hosting
* [New Relic](https://href.li/?https://newrelic.com/termsandconditions/privacy), for service monitoring and log storage

We also archive Discord service data including usernames and message contents on sysop-controlled personal storage devices.

Our security practices surrounding the storage and handling of such data is detailed extensively in our [Operations](../staff-help/operations.md) policy.

## Data removal requests

If for any reason you would like your data removed from our chat services and any copies of it expunged from ancillary systems, you may [submit a request](../policy/guidelines.md#get-in-touch) to an administrator or system operator.

Within 2 weeks, we will carry out your wishes, by expunging chat logs and associated user-identifying metadata from ancillary systems, with some limitations.

We will not delete messages from the Discord server. If you would like this done, you must carry it out yourself using a third-party tool. We strongly recommend against this for chat continuity preservation. If at all possible, consider limiting the scope of deletions, or instead making edits.

We will not remove references to your username from any moderator reports in bot databases or our internal wiki. This information is critical for us to perform our moderation duties, and we must retain it should you use our services again in the future.
