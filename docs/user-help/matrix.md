# Matrix

We self-host a [Matrix](https://href.li/?https://matrix.org) homeserver for those interested in the enhanced privacy afforded through end-to-end encryption.

See the [relevant policy](../policy/matrix.md) specific to this chat service for the AUP and what to expect.

[TOC]

## Access

**Federation is disabled** for the time being. We are entertaining enabling this in the future, but have to look into the consequences of this first. You **must** register an account with our homeserver and log in to your client of choice with it. **Pre-existing accounts** from other homeservers, such as matrix.org, **cannot join** aria rooms!

[**Register**](https://matrix.aria.theater/matrix-registration/register) for an account. Read `#bulletins` on the Discord server for the **token** to enter. Store your credentials some place safe, such as a [password manager](https://privacyguides.org/software/passwords/).

We recommend the cross-platform [Element](https://href.li/?https://element.io/get-started#download) **client**, as it is presently the most feature rich. You may use the [web-version](https://element.aria.theater), which is also self-hosted on our backend server. There are [many other clients](https://href.li/?https://matrix.org/clients/) to explore though, if you prefer another!

To **login** with Element, enter our homeserver address: <https://matrix.aria.theater>. Then, select 'Sign in with Username', and fill in your credentials chosen during sign up.

Once you're in, you ought to be **automatically joined** to `#landing`. You will be briefly interviewed by a staff member for cultural fit at their convenience before being invited to any private rooms you may fancy.

## Encryption

Matrix clients employ end-to-end encryption using a double-ratcheting algorithm based on the [Signal Protocol](https://href.li/?https://en.wikipedia.org/wiki/Signal_Protocol). Encryption is enabled in all private rooms and, by default, direct messages.

On first sign in to Element, you will be prompted to **save your security key**. This allows you to restore your encryption keys when no other active sessions are available for cross-verification. Unfortunately, this distributed model doesn't yet function as a reliable encryption key backup solution. For example, changing your account password resets your security key. Nonetheless, it is useful to back this up.

While we have the rooms configured to allow new users to view all messages since they were created, Synapse and Element [do not yet support](https://href.li/?https://github.com/vector-im/element-web/issues/2286#issuecomment-767105983) proper key exchange on join. Manual key transfer from another member is required to read the backlog.

To manage your encryption keys in Element, tap your profile picture on the top-left, and then 'Security & Privacy.' Under the 'Encryption → Cryptography' section, you'll have the options to import or export E2E room keys from file. Above those buttons is your security key, called the 'Session key,' here. Again, you ought to have both of these secrets backed up somewhere safe and secure if you care about being continuity of chat history!